# gitlab-ci-android

This Docker image contains the Android SDK and most common packages necessary for building Android apps in a CI tool like GitLab CI. Make sure your CI environment's caching works as expected, this greatly improves the build time, especially if you use multiple build jobs.

A `.gitlab-ci.yml` with caching of your project's dependencies would look like this:

```
image: gitlab.com/sukoom1/android-ci:latest

stages:
- build
- test

before_script:
- export GRADLE_USER_HOME=$(pwd)/.gradle
- chmod +x ./gradlew

cache:
  key: ${CI_PROJECT_ID}
  paths:
  - .gradle/

build:
  stage: build
  script:
  - ./gradlew assembleDebug
  artifacts:
    paths:
    - app/build/outputs/apk/app-debug.apk

test:
  stage: test
  script:
      - echo "no" | $ANDROID_HOME/tools/bin/avdmanager create avd -n test -k "system-images;android-29;google_apis;x86_64"
      - echo "no" | $ANDROID_HOME/emulator/emulator -avd test -wipe-data -noaudio -no-window -gpu off -verbose -no-anim -skin 768x1280 -qemu 
      - /helpers/wait-for-avd-boot.sh
      - $ANDROID_HOME/platform-tools/adb install -r app/build/outputs/apk/app-debug.apk
      - $ANDROID_HOME/platform-tools/adb install -r app/build/outputs/apk/app-debug-androidTest-unaligned.apk
      - $ANDROID_HOME/platform-tools/adb shell pm grant [package-name] android.permission.SET_ANIMATION_SCALE
      - $ANDROID_HOME/platform-tools/adb emu screenrecord start test.webm
      - $ANDROID_HOME/platform-tools/adb shell am instrument -w -r -e debug false [package-name]/[test-class] | tee test.log
      - $ANDROID_HOME/platform-tools/adb emu screenrecord stop
      - if grep -q "FAILURES!!!" test.log; then exit 1; fi
  artifacts:
    paths:
      - test.webm
      - test.log
    when: on_failure
    expire_in: 1 week

```

